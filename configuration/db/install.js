var mongoose = require('mongoose')
    , SlideType = mongoose.model('SlideType')
    , User = mongoose.model('User');

// Delete all old slider types
SlideType.remove(function() {

    var slideTypes = [
        {
            real_id: 'html',
            name: 'HTML'
        },
        {
            real_id: 'text',
            name: 'Text'
        },
        {
            real_id: 'image',
            name: 'Image'
        }
    ];

    slideTypes.forEach(function(slideType, i) {

        // Insert the slider type
        console.log("- Inserting SlideType: '" + slideType.real_id + "'");

        new SlideType(slideType).save(function() {

            // Make sure we exit on time
            if (i == slideTypes.length - 1) {

                // Craete user
                var user = new User({
                    username: 'admin',
                    name: 'admin',
                    password: 'admin',
                    role: 'admin',
                    api_key: mongoose.Types.ObjectId()
                });
                user.save(function() {
                    console.log('Done');
                    process.exit(0);
                });

            }

        });
    });

});