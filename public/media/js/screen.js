;(function($){

    var Debug = {
        enabled: true,
        log: function() {
            if (Debug.enabled && typeof console != 'undefined') {
                console.log.apply(console, arguments);
            }
        }
    };

    /**
     * Initialize a screen object
     * @constructor
     */
    var Screen = function() {

        var self = this;
        var socket = io.connect(server_url, { query: { screen: screen_id }, transports: ['websocket'] });

        // We have connected
        socket.on('connect', function() {});

        // Failed to connect
        socket.on('error', function() {
            $('#error').text('Failed to connect').show();
        });

        // Received a message
        socket.on('message', function(type, params) {

            Debug.log('Got message: ' + type + ' with params: ', params);

            if (type == 'showSlide') {
                self.addSlideToQueue(params.slide);
            }

        });

        this.initializeSlider();
    };

    /**
     * Triggered by the Scren object.
     * Initialize the initial "loading" slider and
     * start listening for messages from the slides.
     */
    Screen.prototype.initializeSlider = function() {

        var self = this;
        var frame = $('<iframe>').attr('frameborder', 'false')
            .attr('scrolling', 'no')
            .attr('seamless', 'seamless')
            .attr('sandbox', 'allow-scripts')
            .attr('src', '/public/static_pages/loading.html');

        $('.loading-slide').append(frame);

        // Initiate the slider
        $("#wrapper").spicedSlider();

        addEventListener('message', function(response) {

            // The slide preload is ready
            if (response.data == 'slidePreloadReady') {
                self.showQueuedSlide();
            }
        });
    };

    /**
     * Load a slide into the wrapper and add it to the queue
     * @param slide
     */
    Screen.prototype.addSlideToQueue = function(slide) {

        if (slide == null) {
            slide = { data_dir: 'public/data/templates/no-slides' };
        }

        var frame = $('<iframe>').attr('frameborder', 'false')
            .attr('scrolling', 'no')
            .attr('seamless', 'seamless')
            .attr('sandbox', 'allow-scripts allow-same-origin')
            .attr('src', server_url + '/' + slide.data_dir + '/app.html');

        var container = $('<div>').attr('id', slide.id).addClass('item').addClass('slide-container');
        container.append(frame);

        $('#wrapper').data('spicedSlider').addItem(container)

    };

    /**
     * Start showing the slide that has been queued
     */
    Screen.prototype.showQueuedSlide = function() {

        Debug.log('Sliding to next slide');

        $('#wrapper').data('spicedSlider').slideToNext(function() {
            Debug.log('slideToNext callback triggered');
            $('#wrapper iframe').get(0).contentWindow.postMessage('slideShow', window.location.origin);
        });
    };

    $(document).ready(function() {
        var screen = new Screen();
    });

})(jQuery);
