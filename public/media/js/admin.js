;(function($) {

    $.showSuccess = function(message) {
        $('.main-success-alert .message').html(message);
        $('.main-success-alert').slideDown().delay(3000).slideUp();
    };

})(jQuery);
