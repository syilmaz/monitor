;(function($) {

    $.fn.spicedSlider = function(options) {
        // Create a slider and assign it to the data
        this.data('spicedSlider', new SpicedSlider(this, options));
    };


    var SpicedSlider = function(subject, options) {

        var _self = this;
        var _documentWidth = $(document).width();

        /**
         * Recalculate the wrapper width and resize
         * the inner wrapper as well as the items inside it.
         */
        this.updateVars = function() {

            _documentWidth = $(document).width();
            var wrapperWidth = subject.find('.item').length * _documentWidth;

            subject.find('.item').css('width', _documentWidth + 'px');
            subject.find('.inner-wrapper').css('width', wrapperWidth + 'px');
        };

        /**
         * Add an item to the wrapper
         * @param object
         */
        this.addItem = function(object) {

            // Add the object to the wrapper
            subject.find('.inner-wrapper').append(object);

            // We need to update the variables
            this.updateVars();

            // Return itself
            return this;
        };

        /**
         * Start sliding to the next slide.. slide
         */
        this.slideToNext = function() {

            var callback = arguments[0];

            var innerWrapper = $('.inner-wrapper');

            // Nothing to slide
            if (innerWrapper.find('.item').length <= 1) {
                return;
            }

            // Start animating the slide
            innerWrapper.animate(
                { left: '-' + _documentWidth + 'px' },
                {
                    duration: 800,
                    complete: function() {

                        // Remove the first slide
                        innerWrapper.find('.item:first-child').remove();

                        // Reposition the slide
                        innerWrapper.css('left', '0px');

                        // Trigger the callback when needed
                        if (typeof callback != 'undefined') {
                            callback();
                        }
                    }
                }
            );

        };

        /**
         * We need to keep updating the slides
         * when the screen is resized.
         */
        $(window).resize(function() {
            _self.updateVars();
        });
    };

})(jQuery);
