;(function($) {

    $(document).ready(function() {

        /**
         * Function to be triggered when the slide has been preloaded
         */
        window.slidePreloadReady = function() {
            parent.postMessage('slidePreloadReady', window.location.origin);
        };

        // Call the preload function if there is one
        if (typeof onSlidePreLoad != 'undefined') {
            onSlidePreLoad();
        }
        else {
            // Otherwise we are already done pre loading
            window.slidePreloadReady();
        }

        addEventListener('message', function(message) {

            // A slide is being shown
            if (message.data == 'slideShow') {
                if (typeof onSlideShow != 'undefined') {
                    onSlideShow();
                }
            }
        });
    });

})(jQuery);