/* Imports */
var environment = require('./environment-handler.js').getEnvironment();

/* Constants */
var CONFIG_DIR = '../../configuration/';

/* Cached config data */
var config = null;

/**
 * Return the configuration.
 * @returns {JSON}
 */
exports.getConfiguration = function() {

    if (config == null) {
        this.initializeConfiguration();
    }

    return config;
};

/**
 * Load the configuration and cache it.
 */
exports.initializeConfiguration = function() {
    config = require(CONFIG_DIR + environment + '.json');
};

