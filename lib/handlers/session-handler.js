/**
 * Adds session related helper functions.
 * Such as the ability to store errors and such.
 *
 * @param req
 * @param res
 * @param next
 */
module.exports = function(req, res, next) {

    /**
     * Store an success message in the session
     * @param message
     */
    req.addSuccess = function(message) {
        if (typeof req.session.success == 'undefined') {
            req.session.success = [];
        }

        req.session.success.push(message);
    };

    /**
     * Returns the success messages stored in the session
     * and removes them from the session.
     * @returns {Array}
     */
    req.getSuccess = function() {
        if (typeof req.session.success == 'undefined') {
            return [];
        }

        var success = req.session.success;

        // Empty the messages
        req.session.success = [];

        return success;
    };

    /**
     * Store an error message in the session
     * @param message
     */
    req.addError = function(message) {
        if (typeof req.session.errors == 'undefined') {
            req.session.errors = [];
        }

        req.session.errors.push(message);
    };

    /**
     * Returns the errors stored in the session
     * and removes them from the session.
     * @returns {Array}
     */
    req.getErrors = function() {
        if (typeof req.session.errors == 'undefined') {
            return [];
        }

        // We need to store the errors before returning them
        var errors = req.session.errors;

        // Empty the errors
        req.session.errors = [];

        return errors;
    };

    next();
};