var mongoose = require('mongoose')
    , fs = require('fs')
    , settings = require('../../settings');

// Bootstrap the models
fs.readdirSync(settings.APP_PATH + '/models').forEach(function (file) {
    if (~file.indexOf('.js')) {
        require(settings.APP_PATH + '/models/' + file);
    }
});

/**
 * Connects to the MongoDB and then calls the callback
 * @param {Object} config
 * @param {Function} callback
 */
exports.connect = function(config, callback) {
    var config = config || {}
        , host = config.host || '127.0.0.1'
        , port = config.port || 27017
        , name = config.name || 'monitoring';

    mongoose.connect('mongodb://' + host + ':' + port + '/' + name, callback);
};