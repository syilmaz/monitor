var Winston = require('winston');

/**
 * Instantiate a new logger object
 * @type {exports.Logger}
 */
var logger = new Winston.Logger({
    transports: [
        new Winston.transports.Console({
            level: 'info',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
}) ;

/**
 * An instance of the logger
 * @type {Winston.Logger}
 */
module.exports = logger;

/**
 * Writes messages as info to the logger
 * @type {{write: write}}
 */
module.exports.stream = {
    write: function(message, encoding) {
        logger.verbose(message.replace(/^\s+|\s+$/g, ''));
    }
};