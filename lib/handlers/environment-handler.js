
/* Hold the environment value */
var cached_environment = null;

/**
 * Returns the environment.
 * Normalizes it to prevent invalid values.
 * @returns {String}
 */
exports.getEnvironment = function() {

    if (cached_environment == null) {
        this.initializeEnvironment();
    }

    return cached_environment;
};

/**
 * Initializes the environment and caches it.
 */
exports.initializeEnvironment = function() {

    var env = process.env.NODE_ENV;

    switch (env) {
        case 'production':
            cached_environment = 'production';
            break;

        case 'staging':
            cached_environment = 'staging';
            break;

        default:
            cached_environment = 'development';
            break;
    }
};