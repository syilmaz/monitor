var mongoose = require('mongoose')
    , User = mongoose.model('User');

/**
 * Checks to see if the user has logged in
 * for routes that require a login.
 *
 * Otherwise we can show the page as is.
 *
 * @param req
 * @param res
 * @param next
 */
module.exports = function(req, res, next) {

    // We are already logged in, skip logging in.
    if (req.url == '/admin/login' && (typeof req.session != 'undefined' && typeof req.session.user == 'object')) {
        res.redirect('/admin');
        return;
    }

    if (!req.url.indexOf('/api')) {
        User.findOne({ api_key: req.query.privateToken }, function(err, user) {

            if (err || user == null) {
                return res.sendStatus(401);
            }

            next();
        });
    }
    else {
        if (~req.url.indexOf('/admin') && req.url != '/admin/login'
            && (typeof req.session == 'undefined' || typeof req.session.user != 'object')) {
            res.redirect('/admin/login');
            return;
        }

        if (typeof req.session != 'undefined' && typeof req.session.user == 'object') {
            if (req.session.user.role != 'admin' && ~req.url.indexOf('/admin/users')) {
                res.redirect('/admin');
                return;
            }
        }

        next();
    }
};