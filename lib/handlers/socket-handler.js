var SocketIO = require('socket.io')
    , logger = require('./logger-handler')
    , mongoose = require('mongoose')
    , Screen = mongoose.model('Screen')
    , EventEmitter = require('events').EventEmitter;

var socketServer = null;
var timeout = 10 * 1000;

/**
 * Create a new emitter so that we can work through events
 * @type {EventEmitter}
 */
exports.emitter = new EventEmitter();

/**
 * Initialize the websocket server and listen for connections
 * @param server
 */
exports.listen = function(server) {

    logger.info('Websocket has been initialized');

    socketServer = SocketIO(server);
    socketServer.sockets.on('connection', exports.connection);

    socketServer.set('authorization', function(handshakeData, callback) {

        if (typeof handshakeData._query.screen == 'undefined') {
            return callback('Invalid authentication');
        }

        Screen.find({_id: handshakeData._query.screen}, function(err, screen) {

            if (err) {
                return callback(err);
            }

            // We did find a screen. We are authenticated.
            handshakeData.screen_id = handshakeData._query.screen;
            callback(null, true);
        });
    });
};

/**
 * Called when a new connection comes in
 * @param socket
 */
exports.connection = function(socket) {

    logger.verbose('Websocket, received a connection from: ' + socket.conn.remoteAddress + ' for screen: ' + socket.request.screen_id);

    // Join the room based on the screen id. Each screen is a room,
    socket.join(socket.request.screen_id);

    // Listen for messages
    socket.on('message', exports.message);

    // Emit that we want to get the new slides
    exports.emitter.emit('getFirstSlide', socket);
};

exports.sendMessage = function(type, params) {
    socket.send(type, params);
};

exports.message = function(type, params) {
    // TODO: We received messages
};

exports.showSlide = function(slide) {
    socketServer.to(slide.screen._id + '').emit('message','showSlide', {slide: slide});
};