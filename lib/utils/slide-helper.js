var settings = require('../../settings')
    , fs = require('fs')
    , StringDecoder = require('string_decoder').StringDecoder;

var templateDirectory = settings.ROOT_PATH + '/data/slides/template';

/**
 * Create the necessary directories and files
 * specifically for HTML use.
 * @param {ObjectId} slideId A slide ID needs to be provided so that we know what to create.
 * @param {Function} cb Called as such: function(err, directory);
 */
exports.createInitialHTMLData = function(slideId, cb) {

    var directory = 'public/data/slides/' + slideId;
    var bits = directory.split('/');

    try {
        var decoder = new StringDecoder('utf8');
        var path = '';

        // Create the directories necessary
        for (var i = 0; i < bits.length; i++) {
            path += '/' + bits[i];
            if (!fs.existsSync(settings.ROOT_PATH + path)) {
                fs.mkdirSync(settings.ROOT_PATH + path);
            }
        }

        // Create the files necessary (app.js, app.html, app.css)
        var files = fs.readdirSync(templateDirectory);

        // Write the template content to the directory for the slides
        for (i = 0; i < files.length; i++) {
            var file = files[i];
            var fileContent = decoder.write(fs.readFileSync(templateDirectory + '/' + file));
            fileContent = fileContent.replace('<!-- SLIDE JS -->', '/' + directory + '/app.js');
            fileContent = fileContent.replace('<!-- SLIDE CSS -->', '/' + directory + '/app.css');

            fs.writeFileSync(settings.ROOT_PATH + '/' + directory + '/' + file, fileContent);
        }

        cb(null, directory);
    }
    catch (err) {
        cb(err, null);
    }
};

/**
 * Reads and returns the full data that is needed for an HTML slide
 * @param directory
 * @returns {{html: string, js: string, css: string}}
 */
exports.getHTMLData = function(directory) {

    var data = {
        html: '',
        js  : '',
        css : ''
    };

    var decoder = new StringDecoder('utf8');

    data.html = decoder.write(fs.readFileSync(settings.ROOT_PATH + '/' + directory + '/app.html'));
    data.js = decoder.write(fs.readFileSync(settings.ROOT_PATH + '/' + directory + '/app.js'));
    data.css = decoder.write(fs.readFileSync(settings.ROOT_PATH + '/' + directory + '/app.css'));

    return data;
};

/**
 * Writes the data to the appropriate directory
 * @param directory
 * @param data
 */
exports.writeHTMLData = function(directory, data) {
    fs.writeFileSync(settings.ROOT_PATH + '/' + directory + '/app.html', data.html);
    fs.writeFileSync(settings.ROOT_PATH + '/' + directory + '/app.js', data.js);
    fs.writeFileSync(settings.ROOT_PATH + '/' + directory + '/app.css', data.css);
};

/**
 * Delete a folder recursively
 * @param path
 * @private
 */
var _deleteFolderRecursive = function(path) {

    if (fs.existsSync(path)) {

        fs.readdirSync(path).forEach(function(file) {
            var curPath = path + "/" + file;

            // This is a directory so remove the files in the directory
            if(fs.lstatSync(curPath).isDirectory()) {
                _deleteFolderRecursive(curPath);
            }
            else {
                // Delete the file
                fs.unlinkSync(curPath);
            }
        });

        fs.rmdirSync(path);
    }
};

/**
 * Deletes the HTML data directory
 * @param slideId
 */
exports.deleteHTMLData = function(slideId) {
    _deleteFolderRecursive(settings.ROOT_PATH + '/public/data/slides/' + slideId);
};