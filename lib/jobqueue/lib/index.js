var mongoose = require('mongoose')
    , Screen = mongoose.model('Screen')
    , Slide = mongoose.model('Slide')
    , logger = require('../../handlers/logger-handler')
    , socket = require('../../handlers/socket-handler');

/**
 * A simple job queue object that keeps track
 * of all events happening and allows us to
 * run a pushing job queue instead of a polling one.
 *
 * @param screenId
 * @param callback
 * @constructor
 */
var JobQueue = function(screenId, callback) {

    var self = this;

    this.timer = null;
    this.screen = null;
    this.currentSlide = null;
    this.slideIndex = 1;

    // Load the screen
    Screen.findOne({ _id: screenId }, function(err, screen) {
        self.screen = screen;

        // Load the first slide
        self.showNextSlide(callback);
    });

};

/**
 * Halt the queue by clearing the timer
 */
JobQueue.prototype.haltQueue = function() {

    if (this.timer != null) {
        clearTimeout(this.timer);
    }

};

/**
 * Loads the first slide
 * @param callback
 */
JobQueue.prototype.loadFirstSlide = function(callback) {

    var self = this;

    // Next index is 1
    self.slideIndex = 1;

    // When we're first initializing, we need to load the first slide for this screen
    Slide.findOne({ screen: self.screen._id, visible: 1 })
        .sort({ sort_order: 'asc', _id: 'asc' })
        .populate('screen type')
        .exec(function(err, slide) {

            if (err) {
                logger.error(err);
                return callback();
            }

            callback(slide);
        });
};

/**
 * Start a timer until the next slide should be shown
 */
JobQueue.prototype.waitForNextSlide = function() {

    var self = this;

    this.haltQueue();

    this.timer = setTimeout(function() {
        self.showNextSlide();
    }, self.screen.rotateSpeed * 1000);

};

/**
 * Send a message telling the screen that we can start showing
 */
JobQueue.prototype.showNextSlide = function(callback) {

    var self = this;

    logger.verbose('JobQueue sending message to show next slide');

    Slide.findOne({ screen: this.screen._id, visible: 1 })
        .sort({ sort_order: 'asc', id: 'asc' })
        .skip(self.slideIndex)
        .populate('screen type')
        .exec(function(err, slide) {

            if (err) {
                logger.error(err);
                return;
            }

            self.slideIndex++;

            if (slide == null) {
                self.loadFirstSlide(function(slide) {

                    if (slide == null) {
                        // We don't have any slides, so create a fake one
                        slide = {
                            _id: mongoose.Types.ObjectId(),
                            data_dir: 'public/data/templates/no-slides',
                            screen: self.screen,
                            no_slides: true
                        };

                        // Slide hasn't changed. Keep waiting
                        if (self.currentSlide != null && self.currentSlide._id.equals(slide._id)) {

                            self.waitForNextSlide();

                            if (typeof callback != 'undefined') {
                                callback();
                            }

                            return;
                        }

                        self.currentSlide = slide;
                    }
                    else {
                        // Slide hasn't changed. Keep waiting
                        if (self.currentSlide != null && self.currentSlide._id.equals(slide._id)) {
                            self.waitForNextSlide();

                            if (typeof callback != 'undefined') {
                                callback();
                            }

                            return;
                        }
                    }

                    self.screen = slide.screen;
                    self.currentSlide = slide;

                    socket.showSlide(slide);
                    self.waitForNextSlide();

                    if (typeof callback != 'undefined') {
                        callback();
                    }
                });
            }
            else {

                // Slide hasn't changed. Keep waiting
                try {
                    if (self.currentSlide != null && self.currentSlide._id.equals(slide._id)) {
                        self.waitForNextSlide();

                        if (typeof callback != 'undefined') {
                            callback();
                        }

                        return;
                    }
                } catch(e) {
                    // Catch it just in case
                    logger.error(e);
                }

                self.screen = slide.screen;
                self.currentSlide = slide;

                socket.showSlide(self.currentSlide);
                self.waitForNextSlide();

                if (typeof callback != 'undefined') {
                    callback();
                }
            }

        });
};

/**
 * Halt the screen on the provided slide
 * @param slide
 */
JobQueue.prototype.haltOnSlide = function(slide) {

    this.haltQueue();
    this.currentSlide = slide;
    this.screen = slide.screen;
    this.slideIndex = 1;

    socket.showSlide(this.currentSlide);
};

module.exports = JobQueue;