var JobQueue = require('./lib')
    , mongoose = require('mongoose')
    , Screen = mongoose.model('Screen')
    , _ = require('underscore')
    , socket = require('../handlers/socket-handler');

/* Load handlers */
var config = require('../handlers/configuration-handler').getConfiguration()
    , logger = require('../handlers/logger-handler');

/**
 * We need to store all the queue items
 * @type {{}}
 */
var queues = {};

/**
 * Initialize the job queue.
 * Creates a queue for each screen.
 */
exports.initialize = function(callback) {

    Screen.find(function(err, screens) {

        if (err) {
            logger.error('Failed to initialize the job queue');
            logger.error(err);
            return;
        }

        // Once we're done initializing, we can call the callback
        var done = _.after(screens.length, function() {
            logger.info('Job queue has been initialized with ' + screens.length + ' screen(s)');

            // Listen on the emitter
            socket.emitter.on('getFirstSlide', function(client) {
                client.send('showSlide', {slide: exports.getInstance(client.request.screen_id).currentSlide });
            });

            return callback();
        });

        if (screens.length == 0) {
            return done();
        }

        // Create an instance of the job queue for each screen
        screens.forEach(function (screen) {

            // Create an instance of the screen and trigger done
            exports.createInstance(screen._id, done);
        });
    });

};

/**
 * Create job queue instance for this particular screen
 * @param screenId
 * @param callback
 */
exports.createInstance = function(screenId, callback) {

    if (typeof queues[screenId + ''] == 'undefined') {
        queues[screenId] = new JobQueue(screenId, callback);
    }
    else {
        callback();
    }
};

/**
 * Retrieve the job queue instance for this specific screenId
 * @param screenId
 * @return JobQueue
 */
exports.getInstance = function(screenId) {
    return queues[screenId + ''];
};

/**
 * Remove an instance of a screen
 * @param screenId
 */
exports.removeInstance = function(screenId) {
    if (typeof this.getInstance(screenId) != 'undefined') {
        queues[screenId + ''].haltQueue();
        delete queues[screenId + ''];
    }
};