/**
 * Triggered before the slide is shown.
 * Typically this is used to load external content or to build the page.
 * Once the pre-loading is done, make sure to trigger slidePreloadReady();
 */
function onSlidePreLoad() {

    /**
     * TODO: Pre-load some content before showing the slide
     */

    // When done pre-loading, calling slidePreloadReady is mandatory
    slidePreloadReady();
}

/**
 * Triggered when the slide is shown
 */
function onSlideShow() {

}