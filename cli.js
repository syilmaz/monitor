var config = require('./lib/handlers/configuration-handler').getConfiguration()
    , databaseHandler = require('./lib/handlers/database-handler');

databaseHandler.connect(config.database, function(err) {

    if (err) {
        throw err;
    }

    console.log('Running database scripts');

    // Run the install script
    require('./configuration/db/install');
});