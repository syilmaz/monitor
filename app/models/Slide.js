var mongoose = require('mongoose')
    , Schema = mongoose.Schema
    , SlideTypeSchema = require('./SlideType')
    , SlideType = mongoose.model('SlideType')
    , slideHelper = require('../../lib/utils/slide-helper');

var SlideSchema = new Schema({
    name: String,
    transition_type: String,
    type: {
        type: Schema.Types.ObjectId,
        ref: 'SlideType'
    },
    screen: {
        type: Schema.Types.ObjectId,
        ref: 'Screen'
    },
    data_dir: String,
    sort_order: Number,
    visible: { type: Boolean, default: true }
});

// Validate name
SlideSchema
    .path('name')
    .validate(function (name) {
        return name.length;
    }, 'Name cannot be blank');

/**
 * When we are creating a new slide,
 * we need to create the slide files as well
 */
SlideSchema.pre('save', function(next) {

    if (!this.isNew) {
        return next();
    }

    // We already have a data directory, skip the creation
    if (typeof this.data_dir != 'undefined' && this.data_dir.length > 0) {
        return next();
    }

    var self = this;
    var id = this._id;

    // We need to load the SlideType too
    SlideType.findOne({_id: this.type}, function(err, slideType) {
        if (slideType.real_id == 'html') {
            // Create the directory
            slideHelper.createInitialHTMLData(id, function(err, directory) {
                if (err) {
                    throw err;
                }

                self.data_dir = directory;
                next();
            });
        }
    });
});

mongoose.model('Slide', SlideSchema);
