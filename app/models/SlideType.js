var mongoose = require('mongoose')
    , Schema = mongoose.Schema;

var SlideTypeSchema = new Schema({
    real_id: String,
    name: String
});

mongoose.model('SlideType', SlideTypeSchema);