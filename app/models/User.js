var Mongoose = require('mongoose')
    , Crypto = require('crypto')
    , Schema = Mongoose.Schema;

var UserSchema = new Schema({
    name: String,
    username: String,
    hashed_password: String,
    salt: String,
    role: String,
    api_key: Schema.Types.ObjectId
});

UserSchema
    .virtual('password')
    .set(function(password) {
        this._password = password;
        this.salt = this.makeSalt();
        this.hashed_password = this.encryptPassword(password);
    })
    .get(function() {
        return this._password;
    });

// Validate username
UserSchema
    .path('username')
    .validate(function (username) {
        return username.length;
    }, 'Username cannot be blank');

// Validate password
UserSchema
    .path('hashed_password')
    .validate(function (hashed_password) {
        return hashed_password.length;
    }, 'Password cannot be blank');

UserSchema.methods = {

    /**
     * Make a salt string
     * @returns {String}
     */
    makeSalt: function() {
        return Math.round((new Date().valueOf() * Math.random())) + '';
    },

    /**
     * Encrypts the password
     * @param {String} password
     * @returns {String}
     */
    encryptPassword: function(password) {

        if (!password) {
            return '';
        }

        try {
            return Crypto
                .createHmac('sha1', this.salt)
                .update(password)
                .digest('hex');
        }
        catch (err) {
            return '';
        }
    },

    authenticate: function(plainText) {
        return this.encryptPassword(plainText) == this.hashed_password;
    }

};

Mongoose.model('User', UserSchema);