var mongoose = require('mongoose')
    , Schema = mongoose.Schema;

var ScreenSchema = new Schema({
    name: String,
    rotateSpeed: Number,
    slides: [{
        type: Schema.Types.ObjectId,
        ref: 'Slide'
    }]
});

// Validate screen name
ScreenSchema
    .path('name')
    .validate(function (name) {
        return name.length;
    }, 'Name cannot be blank');

mongoose.model('Screen', ScreenSchema);