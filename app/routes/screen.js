var Express = require('express')
    , router = Express.Router()
    , _ = require('underscore')
    , mongoose = require('mongoose')
    , Screen = mongoose.model('Screen')
    , Slide = mongoose.model('Slide')
    , logger = require('../../lib/handlers/logger-handler')
    , slideHelper = require('../../lib/utils/slide-helper')
    , ObjectId = mongoose.Schema.Types.ObjectId;

/**
 * Routing for GET /screen/:id
 */
router.get('/:id', function(req, res) {

    if (typeof req.params.id == 'undefined' || req.params.id.trim().length == 0) {
        return res.send('Invalid ID');
    }

    Screen.findOne({_id: req.params.id}, function(err, screen) {

        if (err) {
            logger.error(err);
            return res.sendStatus(400);
        }

        if (screen == null) {
            logger.verbose('Could not find screen with id: ' + req.params.id);
            return res.sendStatus(404);
        }

        res.render('frontend/screen/index', {
            title: 'Monitor: ' + screen.name,
            layout: false,
            server_url: req.protocol + '://' + req.get('host'),
            screen_id: req.params.id
        });
    });
});


module.exports = router;