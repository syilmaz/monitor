var Express = require('express')
    , router = Express.Router()
    , Mongoose = require('mongoose')
    , Screen = Mongoose.model('Screen')
    , Slide = Mongoose.model('Slide')
    , logger = require('../../../lib/handlers/logger-handler')
    , jobQueue = require('../../../lib/jobqueue');

router.get('/:screen_id/nextSlide', function(req, res) {

    // Load screen
    Screen.findOne({ _id: req.params.screen_id })
        .populate('slides')
        .exec(function(err, screen) {

            if (err) {
                logger.error(err);
                return res.sendStatus(400);
            }

            // Couldn't find slide
            if (screen == null) {
                return res.sendStatus(404);
            }

            var queueInstance = jobQueue.getInstance(screen._id);

            if (queueInstance != null) {
                queueInstance.showNextSlide(function() {
                    res.sendStatus(200);
                });
            }
            else {
                return res.sendStatus(404);
            }
        });
});

/**
 * POST router for /api/screen/:screen_id/showSlide
 * When called, the screen will only show this slide.
 */
router.post('/:screen_id/showSlide', function(req, res) {

    // Load screen
    Screen.findOne({ _id: req.params.screen_id })
        .populate('slides')
        .exec(function(err, screen) {

            if (err) {
                logger.error(err);
                return res.sendStatus(400);
            }

            // Couldn't find slide
            if (screen == null) {
                return res.sendStatus(404);
            }

            // Find the slide from the screen
            Slide.findOne({ _id: req.body.slideId, screen: req.params.screen_id })
                .populate('screen type')
                .exec(function(err, slide) {

                    if (err) {
                        logger.error(err);
                        return res.sendStatus(400);
                    }

                    if (slide == null) {
                        return res.sendStatus(404);
                    }

                    var queueInstance = jobQueue.getInstance(screen._id);

                    if (queueInstance != null) {
                        queueInstance.haltOnSlide(slide);
                        res.sendStatus(200);
                    }
                    else {
                        return res.sendStatus(404);
                    }
            });
        });
});


module.exports = router;