var Express = require('express')
    , router = Express.Router()
    , fs = require('fs');

// Bootstrap the routes for admin
fs.readdirSync(__dirname + '/').forEach(function (file) {
    if (~file.indexOf('.js') && file != 'index.js' && !fs.lstatSync(__dirname + '/' + file).isDirectory()) {
        router.use('/' + file.replace('.js', ''), require(__dirname + '/' + file));
    }
});

/**
 * API index page
 */
router.get('/', function(req, res) {
    res.sendStatus(400);
});

module.exports = router;