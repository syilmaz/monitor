var express = require('express')
    , router = express.Router()
    , fs = require('fs');

// Load routes
router.use('/admin', require('./admin/index.js'));
router.use('/api', require('./api/index.js'));

// Bootstrap the routes for admin
fs.readdirSync(__dirname + '/').forEach(function (file) {
    if (~file.indexOf('.js') && file != 'index.js' && !fs.lstatSync(__dirname + '/' + file).isDirectory()) {
        router.use('/' + file.replace('.js', ''), require(__dirname + '/' + file));
    }
});

// Default index page
router.get('/', function(req, res) {
    res.sendStatus(200);
});

module.exports = router;