var express = require('express')
    , router = express.Router();

/**
 * Logout the user and redirect back to the index page
 */
router.get('/', function(req, res) {
    if (typeof req.session != 'undefined' && typeof req.session.user == 'object') {
        req.session.destroy();
    }

    res.redirect('/admin');
});

module.exports = router;