var Express = require('express')
    , router = Express.Router()
    , Mongoose = require('mongoose')
    , User = Mongoose.model('User')
    , logger = require('../../../lib/handlers/logger-handler');

/**
 * Handles the POST action when trying to login
 */
router.post('/', function(req, res) {

    // Validate the username
    req.assert('username', 'required')
        .notEmpty();

    // Validate the password
    req.assert('password', 'required')
        .notEmpty();

    // We have failed the validation
    if (req.validationErrors() != null) {
        req.addError('Invalid username or password provided');
        res.redirect('login');
    }
    else {
        User.findOne({ username: req.body.username }, function (err, user) {

            if (err) {
                logger.debug(err);
            }
            else {
                // Login and redirect
                if (user && user.authenticate(req.body.password)) {
                    req.session.user = user;
                    res.redirect('/admin');

                    return;
                }
            }

            req.addError('Invalid username or password provided');
            res.redirect('/admin/login');
        });
    }
});

/**
 * Handles the GET request when trying to login
 */
router.get('/', function(req, res) {
    res.render('admin/pages/login', {
        layout: false,
        errors: req.getErrors()
    });
});

module.exports = router;