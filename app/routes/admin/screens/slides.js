var express = require('express')
    , router = express.Router()
    , mongoose = require('mongoose')
    , Screen = mongoose.model('Screen')
    , Slide = mongoose.model('Slide')
    , SlideType = mongoose.model('SlideType')
    , logger = require('../../../../lib/handlers/logger-handler')
    , slideHelper = require('../../../../lib/utils/slide-helper')
    , _ = require('underscore')
    , jobQueue = require('../../../../lib/jobqueue')
    , fs = require('fs')
    , settings = require('../../../../settings');

/**
 * Router for GET /admin/slides
 */
router.get('/:screen_id/slides', function(req, res) {

    Screen.findOne({ _id: req.params.screen_id })
        .populate({ path: 'slides', options: { sort: { sort_order: 1 } } })
        .exec(function(err, screen) {

            res.render('admin/pages/slides/index', {
                layout: 'admin/layout',
                activeMenu: 'slides',
                session: req.session,
                screen: screen,
                errors: req.getErrors(),
                success: req.getSuccess()
            });

        });
});

/**
 * Router for GET /admin/slides/new
 */
router.get('/:screen_id/slides/new', function(req, res) {

    // Load the slide types for the dropdown
    SlideType.find(function (err, slideTypes) {
        res.render('admin/pages/slides/new', {
            layout: 'admin/layout',
            activeMenu: 'slides',
            session: req.session,

            screenId: req.params.screen_id,
            slideTypes: slideTypes,

            errors: req.getErrors(),
            success: req.getSuccess()
        });
    });
});

/**
 * Router for POST /admin/slides/new
 */
router.post('/:screen_id/slides/new', function(req, res) {

    req.assert('name', 'required')
        .notEmpty();

    req.assert('type', 'required')
        .notEmpty();

    if (req.validationErrors() != null) {
        req.addError('Not all required fields are filled in');
        res.redirect('/admin/screens/' + req.params.screen_id + '/slides/new');
    }
    else {
        var slide = new Slide(req.body);
        slide.screen = req.params.screen_id;

        // Find the screen
        Screen.findOne({_id: req.params.screen_id}, function(err, screen) {
            if (err) {
                logger.error(err);
                return;
            }

            slide.save(function(err) {
                if (err) {
                    logger.error(err);
                    return;
                }

                if (typeof screen.slides == 'undefined') {
                    screen.slides = [];
                }

                screen.slides.push(slide);
                screen.save(function() {

                    var queueInstance = jobQueue.getInstance(req.params.screen_id);

                    // Stop showing the current slide if it's the one we are removing
                    if (typeof queueInstance != 'undefined'
                        && (queueInstance.currentSlide == null || queueInstance.currentSlide.no_slides)) {

                        queueInstance.slideIndex = 1;
                        queueInstance.showNextSlide(function() {
                            req.addSuccess('New slide added!');
                            res.redirect('/admin/screens/' + req.params.screen_id + '/slides');
                        });
                    }
                    else {
                        req.addSuccess('New slide added!');
                        res.redirect('/admin/screens/' + req.params.screen_id + '/slides');
                    }
                });
            });
        });
    }

});


/**
 * Router for GET /admin/slides/edit/:id
 */
router.get('/:screen_id/slides/edit/:id', function(req, res) {

    Slide.findOne({ _id: req.params.id })
        .populate('type')
        .exec(function(err, slide) {

            if (err) {
                logger.error(err);
                return;
            }

            // Load the slide content
            var data = {};
            if (slide.type.real_id == 'html') {
                data = slideHelper.getHTMLData(slide.data_dir);
            }

            var uploadsDir = settings.ROOT_PATH + '/' + slide.data_dir + '/uploads';

            try {
                if (fs.lstatSync(uploadsDir).isDirectory()) {
                    var uploadedFiles = fs.readdirSync(uploadsDir);
                }
                else {
                    var uploadedFiles = [];
                }
            }
            catch(error) {
                var uploadedFiles = [];
            }

            var files = [];

            uploadedFiles.forEach(function(file) {
                files.push({
                    name: file,
                    path: '/' + slide.data_dir + '/uploads/' + file
                })
            });

            res.render('admin/pages/slides/edit/' + slide.type.real_id, {
                layout: 'admin/layout',
                activeMenu: 'slides',
                session: req.session,

                slide: slide,
                data: data,
                files: files,

                errors: req.getErrors(),
                success: req.getSuccess(),

                styles: [],

                scripts: [
                    { src: '/public/media/lib/editor/ace.js' },
                    { src: '/public/media/lib/editor/ext-language_tools.js' },
                    { src: '/public/media/js/jquery.timeago.min.js' }
                ]
            });
        });
});

/**
 * Router for POST /admin/slides/edit/:id
 */
router.post('/:screen_id/slides/edit/:id', function(req, res) {

    Slide.findOne({ _id: req.params.id })
        .populate('type')
        .exec(function(err, slide) {

            if (err) {
                logger.error(err);
                return res.sendStatus(400);
            }

            if (slide.type.real_id == 'html') {

                // Validate that all required fields are present
                req.assert('html', 'required')
                    .notEmpty();

                req.assert('js', 'required')
                    .notEmpty();

                req.assert('css', 'required')
                    .notEmpty();

                if (req.validationErrors() != null) {
                    return res.sendStatus(400);
                }

                // Write the data
                slideHelper.writeHTMLData(slide.data_dir, req.body);
                return res.sendStatus(200);
            }

            res.sendStatus(500);
        });

});


/**
 * Router for GET /admin/slides/deletefile/:id
 */
router.get('/:screen_id/slides/deletefile/:id', function(req, res) {

    Slide.findOne({ _id: req.params.id })
        .populate('type')
        .exec(function(err, slide) {

            if (err) {
                logger.error(err);
                return res.sendStatus(400);
            }

            var file = settings.ROOT_PATH + '/' + slide.data_dir + '/uploads/' + req.query.file;

            if (fs.lstatSync(file).isFile()) {
                req.addSuccess('File deleted');
                fs.unlinkSync(file);
            }

            res.redirect('/admin/screens/' + req.params.screen_id + '/slides/edit/' + req.params.id);
        });

});


/**
 * Router for POST /admin/slides/upload/:id
 */
router.post('/:screen_id/slides/upload/:id', function(req, res) {

    Slide.findOne({ _id: req.params.id })
        .populate('type')
        .exec(function(err, slide) {

            if (err) {
                logger.error(err);
                return res.sendStatus(400);
            }

            var file = req.files['fileToUpload'];

            if (typeof file == 'undefined') {
                return res.sendStatus(404);
            }

            var uploadDir = slide.data_dir + '/uploads';
            var newPath = uploadDir + '/' + file.originalname;

            try {
                fs.lstatSync(uploadDir)
            }
            catch(err) {
                fs.mkdirSync(slide.data_dir + '/uploads', 0777);
            }

            // Remove the file if there is already one
            try {
                if (fs.lstatSync(newPath).isFile()) {
                    fs.unlinkSync(newPath);
                }
            }
            catch (err) {

            }

            fs.renameSync(file.path, newPath);

            req.addSuccess('File added');
            res.redirect('/admin/screens/' + req.params.screen_id + '/slides/edit/' + req.params.id);
        });

});


/**
 * Router for POST /admin/slides/sort_order/:id
 * Update the sort order for this slide
 */
router.post('/:screen_id/slides/sort_order', function(req, res) {

    var slides = req.body.slides;
    var keys = Object.keys(slides);

    // We are done updating the sort order
    var done = _.after(keys.length, function() {
        res.sendStatus(200);
    });

    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var sortOrder = slides[key];

        Slide.update({_id: key}, {sort_order: sortOrder}, done);
    }
});

/**
 * Router for GET /admin/slides/:screen_id/slides/:id/delete
 * Deletes a slide and also removes it from the screen
 */
router.get('/:screen_id/slides/:id/delete', function(req, res) {

    Slide.findOne({ _id: req.params.id })
        .populate('screen')
        .exec(function(err, slide) {

            if (err) {
                logger.error(err);
                return res.sendStatus(400);
            }

            var slideIndex = slide.screen.slides.indexOf(slide._id);

            // Remove the slide if we can find it
            if (slideIndex !== -1) {
                slide.screen.slides.splice(slideIndex, 1);
            }

            slide.screen.save(function(err) {

                if (err) {
                    logger.error(err);
                    return res.sendStatus(400);
                }

                slideHelper.deleteHTMLData(slide._id);

                slide.remove();
                slide.save(function() {

                    var queueInstance = jobQueue.getInstance(req.params.screen_id);

                    // Stop showing the current slide if it's the one we are removing
                    if (typeof queueInstance != 'undefined') {

                        if (typeof queueInstance.currentSlide != 'undefined'
                            && queueInstance.currentSlide._id.equals(slide._id)) {
                            queueInstance.showNextSlide();
                        }
                    }

                    req.addSuccess('Removed slide');
                    res.redirect('/admin/screens/' + req.params.screen_id + '/slides');
                });
            });
        });
});


router.get('/:screen_id/slides/:id/visible/:visible', function(req, res) {

    Slide.findOne({ _id: req.params.id }, function(err, slide) {

        if (err) {
            logger.error(err);
            return res.sendStatus(400);
        }

        if (!slide) {
            return res.sendStatus(404);
        }

        // Update the sort order
        slide.visible = req.params.visible;
        slide.save(function() {
            req.addSuccess('Changed visibility');
            res.redirect('/admin/screens/' + req.params.screen_id + '/slides');
        });
    });

});

module.exports = router;