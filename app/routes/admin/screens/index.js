var Express = require('express')
    , router = Express.Router()
    , fs = require('fs')
    , Mongoose = require('mongoose')
    , Screen = Mongoose.model('Screen')
    , logger = require('../../../../lib/handlers/logger-handler')
    , JobQueue = require('../../../../lib/jobqueue');

router.get('/delete/:id', function(req, res) {
    Screen.findOne({ _id: req.params.id }, function(err, screen) {
        if (screen) {
            screen.remove();
            req.addSuccess('Removed screen');
        }

        res.redirect('/admin/screens');
    });
});

/**
 * GET /admin/screens/edit/:id
 */
router.get('/edit/:id', function(req, res) {

    Screen.findOne({ _id: req.params.id }, function(err, screen) {

        if (screen == null) {
            return res.sendStatus(404);
        }

        res.render('admin/pages/screens/edit', {
            layout: 'admin/layout',
            activeMenu: 'screens',
            session: req.session,
            screen: screen,
            errors: req.getErrors(),
            success: req.getSuccess()
        });
    });
});

/**
 * POST /admin/screens/edit/:id
 */
router.post('/edit/:id', function(req, res) {

    // Validate name
    req.assert('name', 'required')
        .notEmpty();

    // Validate username
    req.assert('rotate_speed', 'required')
        .notEmpty();

    if (req.validationErrors() != null) {
        // We have errors
        req.addError('All fields are required');
        res.redirect('/admin/screens/edit/' + req.params.id);
    }
    else {
        Screen.findOne({ _id: req.params.id }, function(err, screen) {

            if (screen == null) {
                return res.sendStatus(404);
            }

            screen.rotateSpeed = req.body.rotate_speed;
            screen.name = req.body.name;
            screen.save(function() {
                req.addSuccess('Saved!');
                res.redirect('/admin/screens/edit/' + req.params.id);
            });
        });
    }
});

router.post('/add', function(req, res) {

    // Validate name
    req.assert('name', 'required')
        .notEmpty();

    if (req.validationErrors() != null) {
        // We have errors
        req.addError('A name must be provided for a screen');
        res.redirect('/admin/screens');
    }
    else {
        var screen = new Screen(req.body);

        screen.save(function(err) {
            // Failed to save
            if (err) {
                logger.debug(err);
                req.addError('A server-side error occurred');
            }
            else {
                req.addSuccess('Saved!');
            }

            JobQueue.createInstance(screen._id, function() {
                res.redirect('/admin/screens');
            });
        });
    }
});

router.get('/', function(req, res) {
    Screen.find(function(err, screens) {
        res.render('admin/pages/screens/index', {
            layout: 'admin/layout',
            activeMenu: 'screens',
            session: req.session,
            screens: screens,
            errors: req.getErrors(),
            success: req.getSuccess()
        });
    });
});

// Include the slides
router.use('/', require('./slides'));


module.exports = router;