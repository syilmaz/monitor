var Express = require('express')
    , router = Express.Router()
    , Mongoose = require('mongoose')
    , User = Mongoose.model('User')
    , logger = require('../../../lib/handlers/logger-handler');

/**
 * GET /admin/users
 */
router.get('/', function(req, res) {

    res.render('admin/pages/settings/index', {
        layout: 'admin/layout',
        activeMenu: 'users',
        session: req.session,
        errors: req.getErrors(),
        success: req.getSuccess()
    });
});

module.exports = router;