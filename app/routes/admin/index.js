var Express = require('express')
    , router = Express.Router()
    , fs = require('fs');

// Bootstrap the routes for admin
fs.readdirSync(__dirname + '/').forEach(function (file) {
    if (~file.indexOf('.js') && file != 'index.js' && !fs.lstatSync(__dirname + '/' + file).isDirectory()) {
        router.use('/' + file.replace('.js', ''), require(__dirname + '/' + file));
    }
});

router.use('/screens', require('./screens'));

/**
 * Show the admin index page.
 */
router.get('/', function(req, res) {
    return res.redirect('/admin/screens');
});


module.exports = router;