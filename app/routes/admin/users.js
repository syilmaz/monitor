var Express = require('express')
    , router = Express.Router()
    , Mongoose = require('mongoose')
    , User = Mongoose.model('User')
    , logger = require('../../../lib/handlers/logger-handler');

/**
 * GET /admin/users
 */
router.get('/', function(req, res) {

    User.find().sort({ username: 'asc'}).exec(function(err, users) {

        if (err) {
            logger.error(err);
            return res.sendStatus(400);
        }

        res.render('admin/pages/users/index', {
            layout: 'admin/layout',
            activeMenu: 'users',
            session: req.session,
            users: users,
            errors: req.getErrors(),
            success: req.getSuccess()
        });
    });
});

/**
 * GET /admin/users/new
 * Create a new user account
 */
router.get('/new', function(req, res) {
    res.render('admin/pages/users/new', {
        layout: 'admin/layout',
        activeMenu: 'users',
        session: req.session,
        errors: req.getErrors(),
        success: req.getSuccess()
    })
});

/**
 * POST /admin/users/new
 * Save the new user to the database
 */
router.post('/new', function(req, res) {

    // Validate name
    req.assert('name', 'required')
        .notEmpty();

    // Validate username
    req.assert('username', 'required')
        .notEmpty();

    // Validate password
    req.assert('password', 'required')
        .notEmpty();

    if (req.validationErrors() != null) {
        // We have errors
        req.addError('All fields are required');
        res.redirect('/admin/users/new');
    }
    else {
        var user = new User(req.body);
        user.api_key = Mongoose.Types.ObjectId();

        user.save(function(err) {
            // Failed to save
            if (err) {
                logger.debug(err);
                req.addError('A server-side error occurred');
            }
            else {
                req.addSuccess('Saved!');
            }

            res.redirect('/admin/users');
        });
    }

});

/**
 * GET /admin/users/delete/:id
 * Delete a user
 */
router.get('/delete/:id', function(req, res) {

    User.findOne({ _id: req.params.id }, function(err, user) {

        if (err) {
            logger.error(err);
            return res.redirect('/admin/users');
        }

        if (user == null) {
            req.addError('Could not find user');
            return res.redirect('/admin/users');
        }

        user.remove(function() {
            req.addSuccess('Deleted user successfully');
            res.redirect('/admin/users');
        });
    });
});

/**
 * GET /admin/users/edit/:id
 * Edit a user
 */
router.get('/edit/:id', function(req, res) {

    User.findOne({ _id: req.params.id }, function(err, user) {

        if (err) {
            logger.error(err);
            return res.redirect('/admin/users');
        }

        if (user == null) {
            req.addError('Could not find user');
            return res.redirect('/admin/users');
        }

        res.render('admin/pages/users/edit', {
            layout: 'admin/layout',
            activeMenu: 'users',
            user: user,
            session: req.session,
            errors: req.getErrors(),
            success: req.getSuccess()
        })
    });
});

/**
 * POST /admin/users/edit/:id
 * Update the user
 */
router.post('/edit/:id', function(req, res) {

    // Validate name
    req.assert('name', 'required')
        .notEmpty();

    // Validate username
    req.assert('username', 'required')
        .notEmpty();

    if (req.validationErrors() != null) {
        // We have errors
        req.addError('All fields are required');
        res.redirect('/admin/users/edit');
    }
    else {
        User.findOne({ _id: req.params.id }, function(err, user) {

            if (err) {
                logger.error(err);
                return res.redirect('/admin/users');
            }

            if (user == null) {
                req.addError('Could not find user');
                return res.redirect('/admin/users');
            }

            user.name = req.body.name;
            user.username = req.body.username;

            if (typeof req.body.password != 'undefined' && req.body.password.length > 0) {
                user.password = req.body.password;
            }

            user.save(function (err) {
                // Failed to save
                if (err) {
                    logger.debug(err);
                    req.addError('A server-side error occurred');
                }
                else {
                    req.addSuccess('Saved!');
                }

                res.redirect('/admin/users');
            });
        });
    }

});


module.exports = router;