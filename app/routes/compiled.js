var Express = require('express')
    , router = Express.Router()
    , logger = require('../../lib/handlers/logger-handler')
    , fs = require('fs')
    , settings = require('../../settings')
    , crypto = require('crypto')
    , config = require('../../lib/handlers/configuration-handler').getConfiguration();

/**
 * Compiles all javascript files into one single file
 * GET /compiled/js
 */
router.get('/js', function(req, res) {

    // Set the header to text/javascript
    res.setHeader('Content-Type', 'text/javascript');

    var compileFiles = [
        '/public/media/js/jquery-1.7.2.min.js',
        '/public/media/js/spiced.framework.js'
    ];

    var md5 = crypto.createHash('md5');
    md5.update(compileFiles.join(','));

    var fileName = md5.digest('hex') + '.js';
    var filePath = settings.ROOT_PATH + '/public/cache/' + fileName;

    if (config.cache_ignore_js || !fs.existsSync(filePath)) {

        // Create the file
        fs.writeFileSync(filePath, '');

        for (var i = 0; i < compileFiles.length; i++) {
            fs.appendFileSync(filePath, fs.readFileSync(settings.ROOT_PATH + compileFiles[i]));
        }

    }

    return res.send(fs.readFileSync(filePath));
});

module.exports = router;