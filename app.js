/* Global requires */
var express = require('express')
    , morgan = require('morgan')
    , partials = require('express-partials')
    , bodyParser = require('body-parser')
    , session = require('express-session')
    , validator = require('express-validator')
    , FileStore = require('session-file-store')(session)
    , multer = require('multer');

/* Load handlers */
var config = require('./lib/handlers/configuration-handler').getConfiguration()
    , sessionHandler = require('./lib/handlers/session-handler')
    , db = require('./lib/handlers/database-handler')
    , authHandler = require('./lib/handlers/auth-handler')
    , logger = require('./lib/handlers/logger-handler')
    , socketHandler = require('./lib/handlers/socket-handler');

var JobQueue = require('./lib/jobqueue');

var app = express();
var server = require('http').Server(app);

// load the express-partials middleware
app.use(partials());

// load the body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Set the validator
app.use(validator());

// Log requests
app.use(morgan('combined', { stream: logger.stream }));

// Set template engine
app.set('view engine', 'ejs');
app.set('views', './app/views');

// Set the media directory
app.use('/public', express.static(__dirname + '/public', { maxAge: 10 }));

// Set the session
app.use(session({
    secret: config.server.secret,
    resave: false,
    saveUninitialized: false,
    store: new FileStore()
}));

app.use(sessionHandler);

// Add the authentication handler
app.use(authHandler);

// Uploader
app.use(multer({dest: './data/uploads/'}));

// Load routes
app.use('/', require('./app/routes/index.js'));

// Start the server
db.connect(config.database, function(err) {

    // Throw the error if there is one
    if (err) {
        throw err;
    }

    logger.info('Connection with database has been initialized');

    socketHandler.listen(server);

    // We need to initialize the job queue
    JobQueue.initialize(function() {
        server.listen(config.server.port);

        logger.info('Server running on port: ' + config.server.port);
    });
});